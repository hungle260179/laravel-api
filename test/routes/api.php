<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\BlogController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
// Product
Route::get('/product', [ProductController::class,'index']);
Route::get('/product/{id}', [ProductController::class,'show']);
Route::post('/product', [ProductController::class,'store']);
Route::put('/product/{id}',[ProductController::class, 'update']);
Route::delete('/product/{id}', [ProductController::class,'delete']);

//Blog
Route::get('/blogs', [BlogController::class,'index']);
Route::get('/blogs/{id}', [BlogController::class,'show']);
Route::post('/blogs', [BlogController::class,'store']);
Route::put('/blogs/{id}',[BlogController::class, 'update']);
Route::delete('/blogs/{id}', [BlogController::class,'delete']);