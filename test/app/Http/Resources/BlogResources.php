<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BlogResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array //thiet lap moi quan he trong resouce
    {
        return [
            'id' => $this->id,
            'name' => $this->title,
            'shortdescription'=>$this->shortdescription,
            'content' => $this->content,
            'category_id' => $this->category_id,
            'category' => new CategoryResources($this->category), //ket noi category voi  CategoryResources
            'user_id' => $this->user_id,
            'user' => new UserResources($this->user),  //ket noi user voi  UserResources
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}

