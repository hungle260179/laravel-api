<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    //validate
    public function rules(): array
    {
        return [
            'title' => 'required|unique:blogs',
            'content' => 'required',
            'shortdescription' => 'required',
            'category_id' => 'required',
            'user_id' => 'required'
        ];
    }
    //error messages
    public function messages()
    {
        return [
            'title.required' => 'Vui lòng nhập tên title .',
            'content.required' => 'Vui lòng nhập content.',
            'shortdescription.required' => 'Vui lòng nhập description',
            'category_id.required' => 'Vui lòng nhập cateid',
            'title.unique' => 'title đã tồn tại.',
        ];
    }
}
