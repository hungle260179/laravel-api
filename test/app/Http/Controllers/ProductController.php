<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Http\Requests\StorePostRequest;
use App\Http\Resources\ProductResources;

class ProductController extends Controller
{

    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }
    //Get All
    public function index()
    {
        $products = $this->productRepository->getAll();

        return ProductResources::collection($products);
    }
     //Get By ID
    public function show($id)
    {
        $product = $this->productRepository->find($id);
        return new ProductResources($product);
    }
    //Add
    public function store(StorePostRequest $request)
    {
        $product = $this->productRepository->create($request->validated());
        return new ProductResources($product);
    }
    //Update
    public function update(Request $request, $id)
    {
        $product = $this->productRepository;
        $product->update($id, $request->all());

        return new ProductResources($product->find($id));
    }
    //Delete
    public function delete($id)
    {
        $product = $this->productRepository->find($id);

        if (!$product) {
            return response()->json(['message' => 'Product not found'], 404);
        }

        $this->productRepository->delete($id);

        return response()->json(['message' => 'Product deleted']);
    }
}
