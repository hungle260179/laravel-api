<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\BlogResources;
use App\Repositories\Blog\BlogRepositoryInterface;
use App\Http\Requests\BlogRequest;

class BlogController extends Controller
{
    /**
     * construct RepositoryInterface
     */
    protected $blogRepository;
    /**
     * Khởi tạo một instance của BlogController.
     *
     * @param BlogRepositoryInterface $blogRepository
     */
    public function __construct(BlogRepositoryInterface $blogRepository)
    {
        $this->blogRepository = $blogRepository;  // day du lieu tu BlogRepositoryInterface
    }
    /**
     * Lấy tất cả các tài nguyên Blog.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $blog = $this->blogRepository->getAll();
        return BlogResources::collection($blog);
    }

    /**
     * Tạo mới một tài nguyên Blog.
     *
     * @param BlogRequest $request
     * @return BlogResources
     */
    public function store(BlogRequest $request)
    {
        $product = $this->blogRepository->create($request->validated());
        return new BlogResources($product);
    }
    /**
     * Lấy thông tin một tài nguyên Blog cụ thể.
     *
     * @param int $blogId
     * @return BlogResources
     */
    public function show($id)
    {
        $blog = $this->blogRepository->find($id);
        return new BlogResources($blog);
    }
    /**
     * Cập nhật một tài nguyên Blog.
     *
     * @param BlogRequest $request
     * @param int $blogId
     * @return BlogResources
     */
    public function update(BlogRequest  $request, $id)
    {
        $blog = $this->blogRepository->update($id, $request->validated());
        return new BlogResources($blog);
    }
    /**
     * Xóa một tài nguyên Blog.
     *
     * @param int $blogId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $this->blogRepository->delete($id);
        return response()->json(['message' => 'Blog deleted']); // tra ra kieu json
    }
}
