<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */

     //Get All
    public function index()
    {
        return Blog::all();
    }

   

    /**
     * Store a newly created resource in storage.
     */
     //Add
    public function store(Request $request)
    {
        return Blog::create($request->all());
    }

    /**
     * Display the specified resource.
     */
     //Get By ID
    public function show($id)
    {
        return Blog::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     */
     //Update
    public function update(Request $request, $id)
    {
        $blog = Blog::findOrFail($id);
        $blog->update($request->all());

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     */
      //Delete
    public function delete($id)
    {
         $blog= Blog::findOrFail($id);
        $blog->delete();

        return response()->json(['message' => 'Product deleted']);
    }
}
