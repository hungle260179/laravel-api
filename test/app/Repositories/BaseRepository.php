<?php

namespace App\Repositories;

use App\Repositories\RepositoryInterface;

abstract class BaseRepository implements RepositoryInterface
{
    //model muốn tương tác
    protected $model;

    //khởi tạo
    public function __construct()
    {
        $this->setModel();
    }

    //lấy model tương ứng
    abstract public function getModel();

    /**
     * Set model
     */
    public function setModel()
    {
        $this->model = app()->make(
            $this->getModel()
        );
    }
    /**
     * Set model
     */
    public function getAll()
    {
        return $this->model->all();
    }
   /**
 * Lấy thông tin của một tài nguyên Blog theo ID.
 *
 * @param int $id
 * @return \App\Models\Blog|null
 */
    public function find($id)
    {
        $result = $this->model->find($id);

        return $result;
    }
   /**
 * Tạo mới một tài nguyên Blog.
 *
 * @param array $data
 * @return \App\Models\Blog
 */
    public function create($attributes = [])
    {
        return $this->model->create($attributes);
    }
   /**
 * Cập nhật thông tin của mộttài nguyên Blog theo ID.
 *
 * @param int $id
 * @param array $attributes
 * @return bool
 */
    public function update($id, $attributes = [])
    {
        $result = $this->find($id);
        if ($result) {
            $result->update($attributes);
            return $result;
        }

        return false;
    }
  /**
 * Xóa một tài nguyên Blog theo ID.
 *
 * @param int $id
 * @return bool
 */
    public function delete($id)
    {
        $result = $this->find($id);
        if ($result) {
            $result->delete();

            return true;
        }

        return false;
    }
}
