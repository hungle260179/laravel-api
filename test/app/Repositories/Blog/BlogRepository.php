<?php

namespace App\Repositories\Blog;

use App\Repositories\BaseRepository;

class BlogRepository extends BaseRepository implements BlogRepositoryInterface
{
    /**
     * Trả về model tương ứng với repository.
     *
     * @return string
     */
    //lấy model tương ứng
    public function getModel()
    {
        return \App\Models\Blog::class; //ket noi voi Model Blog
    }
    /**
     * Lấy danh sách các tài nguyên Blog.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getBlog()
    {
        return $this->model->select('title')->get();
    }
}
